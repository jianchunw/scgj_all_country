#!/bin/bash

FLAVOR=$1
BUILD_TYPE=$2

#check user input
if [ -z $FLAVOR ]; then
	echo "usage: "$0" [flavor] [debug/release(default)]"
	exit 0
fi
if [ -z $BUILD_TYPE ];then
	BUILD_TYPE="release"
	echo "build type set to "$BUILD_TYPE
fi
RELEASE_CMD="gradle clean assemble"${FLAVOR}${BUILD_TYPE}

#run git command
echo "######refreshing code######"
git clean -f
git pull
echo ""

echo "######building######"
echo "$RELEASE_CMD"
echo ""

#start run gradle cmd
$RELEASE_CMD
#if build failed,exit
if [ ! $? -eq 0 ];then
	echo "error when building"
	exit -1
fi

#locate file
FILE_NAME="app-"${FLAVOR}"-"${BUILD_TYPE}".apk"
FILE_PATH=./app/build/outputs/apk/
FILE_LOCATION=`find ${FILE_PATH} -name *.apk | grep -i ${FILE_NAME}`

echo "######build finish######"
echo "file location: "${FILE_LOCATION}
echo ""

echo "######uploading to fir######"
fir p ${FILE_LOCATION} -T ea5efc3b514663d9ec1398daee3d35d6

if [ $? -eq 0 ];then
	echo "release finish"
else
	echo "fail to upload"
fi
