package com.shicaigj.show.ui.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.shicaigj.show.ui.webview.MyWebView;
import com.shicaigj.show.ui.webview.WVJBWebViewClient;
import com.shicaigj.show.R;
import com.shicaigj.show.utils.Config;
import com.shicaigj.show.utils.LocationHelper;
import com.tendcloud.tenddata.TCAgent;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Otto on 2015/8/7.
 */
public class MainActivity extends Activity implements View.OnClickListener{

    private static final String TAG = "MainActivity";
    @Bind(R.id.my_webview)
    MyWebView webView;
    @Bind(R.id.reload)
    TextView reload;
    @Bind(R.id.turn_home)
    TextView turnHome;
    @Bind(R.id.error_layout)
    LinearLayout errorLayout;
    @Bind(R.id.error_text)
    TextView errorText;

    private LocationHelper locationHelper;
    private MyHandler handler;
    private String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);//去掉标题栏
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initView();
        initListener();

    }

    private void initView(){
        webView.setFocusable(true);
        webView.setFocusableInTouchMode(true);
        webView.requestFocus();
        webView.requestFocusFromTouch();
        handler = new MyHandler(this);
        locationHelper = LocationHelper.getInstance(this, handler);
        webView.setHandler(handler, locationHelper);
        errorLayout.setVisibility(View.GONE);
        /**默认load方式*/
//        webView.loadUrl("file:///android_asset/ExampleApp.html");
        url = Config.SHOW_URL;
        webView.loadUrl(url + "home");

    }

    private void initListener() {
        reload.setOnClickListener(this);
        turnHome.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        if(webView.canGoBack() && !webView.getUrl().equals(url + "home") && !webView.getUrl().equals(url + "category")){
            webView.goBack();
        }else{
            new AlertDialog.Builder(this)
                    .setTitle("提示")
                    .setMessage("确认退出么？")
                    .setPositiveButton("退出",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    dialog.cancel();
                                    finish();
                                }
                            })
                    .setNegativeButton("再逛逛",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    dialog.cancel();
                                }
                            }).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        TCAgent.onResume(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.reload:
                webView.loadUrl(webView.getUrl());
                errorLayout.setVisibility(View.GONE);
                webView.setVisibility(View.GONE);
                break;
            case R.id.turn_home:
//                webView.loadUrl("file:///android_asset/ExampleApp.html");
                webView.loadUrl(url + "home");
                errorLayout.setVisibility(View.GONE);
                webView.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
       switch (resultCode){
           case 0:
               LocationHelper location = LocationHelper.getInstance(MainActivity.this, handler);
               location.updateLocation();
               break;
       }
    }

    class MyHandler extends Handler{
        WeakReference<MainActivity> mActivity;

        MyHandler(MainActivity activity) {
            mActivity = new WeakReference<MainActivity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            final MainActivity theActivity = mActivity.get();
            switch (msg.what){
                case Config.WEBVIEW_ERROR_TYPE:
                    errorLayout.setVisibility(View.VISIBLE);
                    webView.setVisibility(View.GONE);
                    int errorCode = (int) msg.obj;
                    String errInfo = "";
                    switch (errorCode){
//                        case -2:
//                        case -14:
//                            errorText.setText(getResources().getString(R.string.webview_no_find_err));
//                            break;
//                        case -8:
//                            errorText.setText(getResources().getString(R.string.webview_time_out));
//                            break;
                        case 0:
                            errInfo = getResources().getString(R.string.webview_0);
                            break;
                        case 404:
                            errInfo = getResources().getString(R.string.webview_404);
                            break;
                        case 500:
                            errInfo = getResources().getString(R.string.webview_500);
                            break;
                        case 502:
                            errInfo = getResources().getString(R.string.webview_502);
                            break;
                        default:
//                            errorText.setText(getResources().getString(R.string.webview_net_err));
                            errInfo = "发生错误";
                            break;
                    }
                    errorText.setText(errInfo + "\n错误码：" + errorCode);
                    break;
                case Config.WEBVIEW_START_LOAD_TYPE:
                    webView.setVisibility(View.VISIBLE);
                    break;
                case Config.WEBVIEW_OPEN_GPS:
                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle("提示")
                            .setMessage("GPS没开，是否要开启？")
                            .setPositiveButton("否",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog,
                                                            int which) {
                                            dialog.cancel();
                                        }
                                    })
                            .setNegativeButton("是",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog,
                                                            int which) {
                                            dialog.cancel();
                                            // 返回开启GPS导航设置界面
                                            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                            theActivity.startActivityForResult(intent, 0);
                                        }
                                    }).show();
                    break;
                case Config.WEBVIEW_LOCATION_SUCCESS:
                    String message = (String) msg.obj;
                    try {
                        Log.i(TAG, message);
                        webView.webViewClient.callHandler("LocationSuccess", new JSONObject(message), new WVJBWebViewClient.WVJBResponseCallback() {

                            @Override
                            public void callback(Object data) {
                                Log.i(TAG, "location send success!!!!");
                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        TCAgent.onPause(this);
    }
}
