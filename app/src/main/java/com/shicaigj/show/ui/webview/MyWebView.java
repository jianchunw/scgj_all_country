package com.shicaigj.show.ui.webview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.WindowManager;
import android.webkit.ConsoleMessage;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Toast;

import com.shicaigj.show.application.MyApplication;
import com.shicaigj.show.ui.dialog.PhoneDialog;
import com.shicaigj.show.ui.dialog.LoadDataDialog;
import com.shicaigj.show.utils.Config;
import com.shicaigj.show.utils.LocationHelper;

import java.util.HashMap;
import java.util.Map;

public class MyWebView extends WebView {

    private static final String TAG = "MyWebView";
    private Context context;
    protected PackageManager pm;
    private LoadDataDialog dialog;
    private Handler handler = null;


    public WVJBWebViewClient webViewClient;
    private LocationHelper locationHelper;

    public MyWebView(Context context) {
        this(context, null, 0);
    }

    public MyWebView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    @SuppressLint("NewApi")
    public MyWebView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }
        this.context = context;
        pm = context.getPackageManager();
        init();
    }


    /**
     * 定时结束dialog框
     */
    @SuppressLint("HandlerLeak")
    private Handler doActionHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            int msgId = msg.what;
            switch (msgId) {
                case 1:
                    if (null != dialog && dialog.isShowing()) {
                        dialog.Cancel();
                    }
                    break;
                default:
                    break;
            }
        }
    };

    WebSettings webSettings;

    @SuppressLint("SetJavaScriptEnabled")
    public void init() {
        webSettings = this.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setRenderPriority(WebSettings.RenderPriority.HIGH);
        webSettings.setBlockNetworkImage(true);
        webSettings.setCacheMode(WebSettings.LOAD_DEFAULT);
        webSettings.setDomStorageEnabled(true);
        webSettings.setSavePassword(false);
        webSettings.setSaveFormData(false);

        /**禁用android5.0硬件加速*/
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.LOLLIPOP) {
             Log.d(TAG,"SDK_INT >= 21");
        }else{
             Log.d(TAG,"SDK_INT < 21");
        }

        WebChromeClient webChromeClient = new WebChromeClient() {
            @Override
            public boolean onJsAlert(WebView view, String url, String message,
                                     JsResult result) {
                Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                return super.onJsAlert(view, url, message, result);
            }

            public boolean onConsoleMessage(ConsoleMessage cm) {
                 Log.d(TAG,cm.message() + " -- From line " + cm.lineNumber()
                                + " of " + cm.sourceId());
                return true;
            }
        };
        this.setWebChromeClient(webChromeClient);
        webViewClient = new MyWebViewClient(this);
        webViewClient.enableLogging();
        this.setWebViewClient(webViewClient);
    }

    /**
     * 同步一下cookie
     */
    public void synCookies(Context context, SharedPreferences sp) {
    }

    @Override
    public void reload() {
        super.reload();
    }

    /**
     * 重置isFirst，显示小菊花
     */
    public void resetIsFirst(){
    }

    /**
     * 得到mainActivity中的handler
     */
    public void setHandler(Handler handler, LocationHelper locationHelper){
        this.handler = handler;
        this.locationHelper = locationHelper;
    }

    @Override
    public void loadUrl(String url) {
        Map<String, String> headers = null;
        headers = new HashMap<String, String>();
        headers.put("SCGJ-pt", "android");
        headers.put("SCGJ-os", System.getProperty("os.name"));
        headers.put("SCGJ-version", MyApplication.versionName);
        this.loadUrl(url, headers);
    }

    class MyWebViewClient extends WVJBWebViewClient {

        public MyWebViewClient(final WebView webView) {
            super(webView, new WVJBWebViewClient.WVJBHandler() {

                @Override
                public void request(Object data, WVJBResponseCallback callback) {
                    callback.callback("Response for message from ObjC!");
                }
            });
			/*
			// not support js send
			super(webView);
			*/

            enableLogging();

            registerHandler("StartLocation", new WVJBWebViewClient.WVJBHandler() {

                @Override
                public void request(Object data, WVJBResponseCallback callback) {
                    locationHelper.updateLocation();
                    callback.callback("已经开始请求位置信息");
                }
            });

        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            //尝试解决android.view.WindowManager$BadTokenException
            if (null == dialog) {
                dialog = LoadDataDialog.getInstance(context);
            }
            if (!dialog.isShowing()) {
                try {
                    dialog.show();
                } catch (WindowManager.BadTokenException e) {
                    e.printStackTrace();
                    Log.e(TAG,  "BadTokenException!!!");
                }
            }
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            webSettings.setBlockNetworkImage(false);
            if (null != dialog && dialog.isShowing()) {
                dialog.Cancel();
            }
            handler.sendEmptyMessage(Config.WEBVIEW_START_LOAD_TYPE);
            super.onPageFinished(view, url);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            //如url以tel:开头则拨打电话
            if (url.startsWith("tel:")) {
                String num = url.substring(4, url.length());
                PhoneDialog phoneDialog = new PhoneDialog(context, num);
                phoneDialog.showDialog();
            }else if (url.contains("mailto")) {   //如url以mailto:打开手机中邮箱
                // TODO: extract the email... that's your work, LOL
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    context.startActivity(intent);
                }
                catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(context, "你手机没有发现邮件相关应用！", Toast.LENGTH_SHORT).show();
                }
            }else if(url.equals("wvjbscheme://__WVJB_QUEUE_MESSAGE__")) { //处理bridge url 特别的问题
                return super.shouldOverrideUrlLoading(view, url);
            }else {
                return false;
            }
            return true;
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            Log.d(TAG, "description = " + description);
            super.onReceivedError(view, errorCode, description, failingUrl);
            //用html隐藏系统定义的404页面信息
            Message msg = Message.obtain();
            msg.what = Config.WEBVIEW_ERROR_TYPE;
            msg.obj = errorCode;
            handler.sendMessage(msg);
        }
    }

}
