package com.shicaigj.show.ui.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.shicaigj.show.R;


public class LoadDataDialog extends Dialog {
	
	private static LoadDataDialog loadDataDialog = null;
    private static final Object lock = new Object();
    private ImageView gifView;
    private AnimationDrawable animationDrawable;
    private Context context;

    private LoadDataDialog(Context context) {
        super(context, R.style.load_data_style);
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.load_data, null);
        gifView = (ImageView) view.findViewById(R.id.loading_dialog_iv);
        animationDrawable = (AnimationDrawable) gifView.getBackground();
        gifView.post(new Runnable() {
            @Override
            public void run() {
                animationDrawable.start();
            }
        });
        // 设置Gif图片源
//        gifView.setGifImage(R.drawable.loading_80);
//        gifView.setGifImageType(GifView.GifImageType.COVER);
        setContentView(view);
        setCanceledOnTouchOutside(false);
        setCancelable(true);
        this.context = context;
    }
    
    public static LoadDataDialog getInstance(Context context){
    	if (loadDataDialog == null) {
            synchronized (lock) {
                if(loadDataDialog == null) {
                    loadDataDialog = new LoadDataDialog(context);
                }
            }
        }
		return loadDataDialog;
    }

    public void Show() {
        try {
            super.show();
        }catch (WindowManager.BadTokenException e){
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public void Cancel() {
        if(context != null && context instanceof Activity){
            if(!((Activity) context).isFinishing()){
                try {
                    super.cancel();
                }catch (IllegalArgumentException e){
                    e.printStackTrace();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

        loadDataDialog = null;
        animationDrawable.stop();
    }
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
    	 if (keyCode == KeyEvent.KEYCODE_BACK) {
             Cancel();
    	 }
    	return super.onKeyDown(keyCode, event);
    }
}
