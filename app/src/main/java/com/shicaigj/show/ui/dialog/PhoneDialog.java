package com.shicaigj.show.ui.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;

import com.shicaigj.show.R;


/**
 * 客服电话dialog
 * Created by Otto on 2015/3/6.
 */
public class PhoneDialog {

    private AlertDialog.Builder builder;

    public void setAfterCalledListener(AfterCalledListener afterCalledListener) {
        this.afterCalledListener = afterCalledListener;
    }

    private AfterCalledListener afterCalledListener;

    public PhoneDialog(final Context context, final String number) {
        builder = new AlertDialog.Builder(context);
        builder.setTitle(number);
        builder.setPositiveButton(context.getResources().getString(R.string.about_us_dialog_call_positive),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent phoneIntent = new Intent("android.intent.action.CALL",
                                Uri.parse("tel:" + number));
                        context.startActivity(phoneIntent);
                        if(afterCalledListener != null){
                            afterCalledListener.afterCalled();
                        }
                    }
                });
        builder.setNegativeButton(context.getResources().getString(R.string.dialog_cancel_btn_text),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

    }

    public void showDialog(){
        builder.create().show();
    }

    public interface AfterCalledListener{
        public void afterCalled();
    }
}
