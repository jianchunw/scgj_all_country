/*
 * File Name: MyApplication.java 
 * History:
 * Created by YiTao on 2014-9-22
 */
package com.shicaigj.show.application;

import android.app.Application;
import android.content.pm.PackageManager;

import com.shicaigj.show.R;
import com.shicaigj.show.utils.Config;
import com.tendcloud.tenddata.TCAgent;


public class MyApplication extends Application {
    // ==========================================================================
    // Constants
    // ==========================================================================
    private static final String TAG = "LeyiApplication";
    // ==========================================================================
    // Fields
    // ==========================================================================
    public static String versionName = null;

    public boolean isTestVersion = false;

    @Override
    public void onCreate() {
        super.onCreate();

        if ("食材管家测试版".equals(getString(R.string.app_name))) {
            Config.SHOW_URL = Config.SHOW_IP_TEST;
            isTestVersion = true;
        } else {
            Config.SHOW_URL = Config.SHOW_IP_ONLINE;
            isTestVersion = false;
        }

        try {
            versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        TCAgent.LOG_ON = false;
        TCAgent.init(this);
        TCAgent.setReportUncaughtExceptions(true);
    }
}