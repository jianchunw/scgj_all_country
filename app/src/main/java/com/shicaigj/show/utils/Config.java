/*
 * File Name: Config.java 
 * History:
 * Created by YiTao on 2014-9-29
 */
package com.shicaigj.show.utils;

import java.util.ArrayList;
import java.util.List;

public class Config {
    // ==========================================================================
    // Constants
    // ==========================================================================


    /**
     * 线上
     */
    public static String SHOW_URL = "http://test.shicaiguanjia.cn/";
    public static String SHOW_IP_ONLINE= "http://g.shicaiguanjia.com/";

    /**
     * 测试
     */
    public static final String SHOW_IP_TEST = "http://119.254.102.122:1337/";


    public static final int WEBVIEW_ERROR_TYPE = 0x01;
    public static final int WEBVIEW_START_LOAD_TYPE = 0x02;
    public static final int WEBVIEW_LOCATION_SUCCESS = 0x03;
    public static final int WEBVIEW_OPEN_GPS = 0x04;

}
