package com.shicaigj.show.utils;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.location.LocationManagerProxy;
import com.amap.api.location.LocationProviderProxy;

import java.io.IOException;
import java.util.List;


/**
 * Created by chuanyeou on 15/4/21.
 */
public class LocationHelper implements AMapLocationListener {
    private static final String TAG = "LocationHelper";
    private LocationManagerProxy mLocationManagerProxy;
    private static LocationHelper instance;
    private Context context;
    private Handler mainHandler;


    private LocationHelper(Context context, Handler mainHandler) {
        this.context = context;
        this.mainHandler = mainHandler;
        mLocationManagerProxy = LocationManagerProxy.getInstance(context);
        mLocationManagerProxy.setGpsEnable(true);
    }

    public static LocationHelper getInstance(Context context, Handler mainHandler) {
        if (null ==  instance) {
            instance = new LocationHelper(context.getApplicationContext(), mainHandler);
        }
        return instance;
    }

    public void updateLocation() {
        LocationManager alm = (LocationManager) context
                .getSystemService(Context.LOCATION_SERVICE);
        if (alm
                .isProviderEnabled(android.location.LocationManager.GPS_PROVIDER)) {
            mLocationManagerProxy.requestLocationData(
                    LocationProviderProxy.AMapNetwork, -1, 1, this);
            mLocationManagerProxy.setGpsEnable(true);
        }else {
            Toast.makeText(context, "请开启GPS导航...", Toast.LENGTH_SHORT).show();
            mainHandler.sendEmptyMessage(Config.WEBVIEW_OPEN_GPS);
        }
    }

    @Override
    public void onLocationChanged(AMapLocation aMapLocation) {
        if (aMapLocation != null && aMapLocation.getAMapException().getErrorCode() == 0) {
            String message = "{\"province\": \"" + aMapLocation.getProvince()
                        + "\",\"city\": \"" + aMapLocation.getCity()
                        + "\", \"coordinate\": \"" + aMapLocation.getLongitude() + "," + aMapLocation.getLatitude()
                        + "\"  }";
            Log.d(TAG, message);
            Message msg = Message.obtain();
            msg.what = Config.WEBVIEW_LOCATION_SUCCESS;
            msg.obj = message;
            mainHandler.sendMessage(msg);
        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

}
